import spidev # To communicate with SPI devices
from numpy import interp    # To scale values
from time import sleep  # To add delay
from simple_pid import PID
import time
import ADS1256
import DAC8532
from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
import datetime as dt
from collections import deque
import numpy as np

ADC = ADS1256.ADS1256()
ADC.ADS1256_init()
DAC = DAC8532.DAC8532()
DAC.DAC8532_Out_Voltage(DAC8532.channel_A, 60)

# Test the PID
MIN_OUTPUT, MAX_OUTPUT = 15, 100
setpoint_pressure = 1  # mbar
pid = PID(5, 2.5, 0.05, setpoint=setpoint_pressure, sample_time=0.1, output_limits=[MIN_OUTPUT, MAX_OUTPUT])
fig, axs = plt.subplots(1, 3, figsize=(20, 10))
num_points = 500
xs, ys_pt1, ys_pt2, ys_pump = deque([], num_points), deque([], num_points), deque([], num_points), deque([], num_points)
ln, = axs[0].plot(xs, ys_pt1, 'C0')
ln2, = axs[1].plot(xs, ys_pt2, 'C1')
ln3, = axs[2].plot(xs, ys_pump, 'C2')
axs[0].axhline(setpoint_pressure, linestyle='--', color='r')
axs[0].set_title('PT1')
axs[0].set_ylim(-20, 20)
axs[1].set_ylim(0, 400)
axs[1].set_title('PT2')
axs[2].set_ylim(0, 100)
axs[2].set_title('Pump speed [%]')

def get_pressure(voltage, sensor=[-20, 20]):
    """Calculate the pressure from the adc using a conversion
    adc -> volts -> currents -> mbar"""
    pressure = interp(voltage, [4, 20], sensor)
    return pressure

def update_plot(frame):
    pt1 = np.interp(ADC.ADS1256_GetChannalValue(5), [0, 2**23 - 1], [0, 20])
    pt1 = np.interp(pt1, [4, 20], [-20, 20])
    output = pid(pt1)
    pump_speed = (MAX_OUTPUT -(output - MIN_OUTPUT))
    print(f'Pump speed: {pump_speed:.0f} - PT01: {pt1:.1f}') 
    pump_voltage = np.interp(pump_speed, [0, 100], [0, 5])
    DAC.DAC8532_Out_Voltage(DAC8532.channel_A, pump_voltage)
    
    pt2 = np.interp(ADC.ADS1256_GetChannalValue(6), [0, 2**23 - 1], [0, 20])
    pt2 = np.interp(pt2, [4, 20], [-1000, 1000])
    # adc_values = np.array(ADC.ADS1256_GetAll())*5.0/0x7fffff
    # pt1 = get_pressure(adc_values[5], sensor=[-20, 20])
    # pt2 = get_pressure(adc_values[6], sensor=[-1000, 1000])
    xs.append(dt.datetime.now().timestamp())
    ys_pt1.append(pt1)
    ys_pt2.append(pt2)
    ys_pump.append(pump_speed)
    ln.set_data(xs, ys_pt1)
    ln2.set_data(xs, ys_pt2)
    ln3.set_data(xs, ys_pump)
    axs[0].relim()
    axs[1].relim()
    axs[2].relim()
    axs[0].autoscale_view()
    axs[1].autoscale_view()
    axs[2].autoscale_view()
    return ln, ln2, ln3
    

# main loop for plotting
try:
    animation = FuncAnimation(fig, update_plot, interval=500)
    plt.show()
except:
    print('Shutting down pump')
    DAC.DAC8532_Out_Voltage(DAC8532.channel_A, 0)
    

# main loop for control
'''
while True:
    adc = get_adc(channel_pt_sensor)
    pressure = get_pressure(adc)
    if pressure > 2:
        set_pump(80)
    else:
        set_pump(40)
    print('Pressure: {:.2f}'.format(pressure))
    time.sleep(0.2)


pump.stop()
IO.cleanup()
'''
