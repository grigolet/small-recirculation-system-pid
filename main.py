import spidev # To communicate with SPI devices
from numpy import interp    # To scale values
from time import sleep  # To add delay
import RPi.GPIO as IO # To use GPIO pins
from simple_pid import PID
import time
from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
import datetime as dt
from collections import deque
IO.setmode(IO.BOARD)
IO.setwarnings(False)
IO.setup(12, IO.OUT)

channel_pt_sensor = 0
# Start SPI connection
spi = spidev.SpiDev() # Created an object
spi.open(0,0)
# Start pwm on channel 12 to control pump
pump = IO.PWM(12, 1000)
pump.start(0)
# Test the PID
# pid = PID(1, 0.1, 0.05, setpoint=1, sample_time=0.1, output_limits=[0, 100])
fig, axs = plt.subplots(1, 2, figsize=(12, 6))
xs, ys_pt1, ys_pt2 = deque([], 100), deque([], 100), deque([], 100)
ln, = axs[0].plot(xs, ys_pt1)
ln2, = axs[1].plot(xs, ys_pt2)
axs[0].set_title('PT1')
axs[1].set_title('PT2')

def set_pump(value):
    pump.ChangeDutyCycle(value)

# Read MCP3008 data
def get_adc(channel):
  spi.max_speed_hz = 1350000
  adc = spi.xfer2([1,(8+channel)<<4,0])
  data = ((adc[1]&3) << 8) + adc[2]
  return data

def get_pressure(adc, resistance=0.206, sensor=[-20, 20]):
    """Calculate the pressure from the adc using a conversion
    adc -> volts -> currents -> mbar"""
    print(adc)
    voltage = interp(adc, [0, 1023], [0, 3.3])
    current = voltage / resistance
    pressure = interp(current, [4, 20], sensor)
    return pressure

def update_plot(frame):
    pt1 = get_pressure(get_adc(0), sensor=[-20, 20])
    pt2 = get_pressure(get_adc(2), sensor=[-1000, 1000])
    xs.append(dt.datetime.now().timestamp())
    ys_pt1.append(pt1)
    ys_pt2.append(pt2)
    ln.set_data(xs, ys_pt1)
    ln2.set_data(xs, ys_pt2)
    axs[0].relim()
    axs[1].relim()
    axs[0].autoscale_view()
    axs[1].autoscale_view()
    return ln, ln2
    

# main loop for plotting
animation = FuncAnimation(fig, update_plot, interval=500)
plt.show()

# main loop for control
'''
while True:
    adc = get_adc(channel_pt_sensor)
    pressure = get_pressure(adc)
    if pressure > 2:
        set_pump(80)
    else:
        set_pump(40)
    print('Pressure: {:.2f}'.format(pressure))
    time.sleep(0.2)


pump.stop()
IO.cleanup()
'''